#include <hdfswrapper.h>
#include <string.h>

namespace hancyber::open::forensic::hdfs
{
    hdfsWrapper* hdfsWrapper::_instance = NULL;
    pthread_mutex_t hdfsWrapper::_singleton_mutex = PTHREAD_MUTEX_INITIALIZER;
    int hdfsWrapper::_ref = 0;

    hdfsWrapper* hdfsWrapper::getInstance(const char* namenode, const char* username)
    {
        pthread_mutex_lock(&_singleton_mutex);
        if (_ref++ == 0 && _instance == NULL)
        {
            _instance = new hdfsWrapper();
            _instance->init(namenode, username);
        }
        pthread_mutex_unlock(&_singleton_mutex);

        return _instance;
    };
    void hdfsWrapper::init(const char* namenode, const char* username)
    {
        hdfsBuilder* builder = hdfsNewBuilder();
        if (builder)
        {
            hdfsBuilderSetNameNode(builder, namenode);
            hdfsBuilderSetUserName(builder, username);
            _fs = hdfsBuilderConnect(builder);
        }
    };
    void hdfsWrapper::release(void)
    {
        pthread_mutex_lock(&_singleton_mutex);
        if (_instance && --_ref == 0)
        {
            if (_fs)
            {
                hdfsDisconnect(_fs);
                _fs = NULL;
            }
            delete _instance;
            _instance = NULL;
        }
        pthread_mutex_unlock(&_singleton_mutex);
    };

    const hdfsFS hdfsWrapper::gethdfsFS(void) const
    {
        return _fs;
    };

    __finfo* hdfsWrapper::read(const char* src) const
    {
        pthread_mutex_lock(&_singleton_mutex);
        __finfo* finfo = NULL;
        if (_instance)
        {
            if (_fs)
            {
                hdfsStreamBuilder* sb = hdfsStreamBuilderAlloc(_fs, src, O_RDONLY);
                if (sb)
                {
                    if (hdfsExists(_fs, src) == 0)
                    {
                        hdfsFile fh = hdfsStreamBuilderBuild(sb);
                        
                        int full_size = hdfsAvailable(_fs, fh);
                        unsigned char* buffer = new unsigned char[full_size + 1];
                        memset(buffer, 0x00, sizeof(char) * (full_size + 1));
                        uint64_t total_size = 0;
                        tSize buffer_size = _READ_BUFFER_SIZE;
                        tSize read_bytes = buffer_size;
                        bool is_first = true;
                        while (read_bytes == buffer_size)
                        {
                            unsigned char* tmp_buffer = new unsigned char[buffer_size];
                            memset(tmp_buffer, 0x00, sizeof(unsigned char) * (buffer_size));
                            read_bytes = hdfsRead(_fs, fh, tmp_buffer, buffer_size);
                            if (read_bytes > 0)
                            {
                                for (int i = 0;i < read_bytes; ++i)
                                {
                                    buffer[total_size + i] = tmp_buffer[i];
                                }
                                /*
                                원하는 동작이 되지 않음...
                                임시로 위의 코드로 대처함
                                if (is_first)
                                {
                                    strcpy((char*)buffer, (const char*)tmp_buffer);
                                    is_first = false;
                                }
                                else
                                {                                                  
                                    strcat((char*)buffer, (const char*)tmp_buffer);
                                }
                                */
                                total_size += read_bytes;
                            }
                            delete[] tmp_buffer;
                        }

                        hdfsCloseFile(_fs, fh);

                        finfo = new __finfo(buffer, total_size);
                    }
                    else
                    {
                        hdfsStreamBuilderFree(sb);
                    }                    
                }
            }
        }
        pthread_mutex_unlock(&_singleton_mutex);
        
        return finfo;        
    };

    int hdfsWrapper::write(const char* dst, unsigned char* data, int f_size) const
    {
        pthread_mutex_lock(&_singleton_mutex);
        int ret = 0;
        if (_instance)
        {
            if (_fs)
            {
                hdfsStreamBuilder* sb = hdfsStreamBuilderAlloc(_fs, dst, O_WRONLY);
                if (sb)
                {
                    hdfsFile fh = hdfsStreamBuilderBuild(sb);
                    ret = hdfsWrite(_fs, fh, data, f_size);
                    hdfsCloseFile(_fs, fh);                    
                }
            }
        }
        pthread_mutex_unlock(&_singleton_mutex);

        return ret; 
    };
}