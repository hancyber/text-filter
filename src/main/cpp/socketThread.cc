#include <socketThread.h>
#include <textextractor.h>

#include <string.h>
#include <string>
#include <iostream>
#include <sys/socket.h>
#include <unistd.h>
#include <vector>



using namespace std;

namespace hancyber::open::forensic
{
    socketThread::socketThread(int sockfd): _sockfd(sockfd), _extractor(new textExtractor()), _extractor_class(new textClass()){};
    socketThread::~socketThread(void)
    {
        if (_sockfd > 0)
        {
            close(_sockfd);
            std::cout << "socket closed" << std::endl;
        }
        if (_extractor)
        {
            delete _extractor;
            _extractor = NULL;
        }
        if (_extractor_class)
        {
            delete _extractor_class;
            _extractor_class = NULL;
        }
    };

    THREADID socketThread::threadID(void) const
    {
        return THREADID::DERIVED;
    };

    const string socketThread::textProcessing(string recv_msg) const
    {
        int f_size = -1;
        string return_msg = to_string(f_size) + _BUCKET_RETURN_SEPARATOR + to_string((int)ERROR_CODE::DEFAULT_ERROR) + _BUCKET_RETURN_SEPARATOR + _DEFAUKT_ERROR_MESSAGE;

        size_t pos = 0;
        vector<string> token;

        while ((pos = recv_msg.find(_BUCKET_ETC_SEPARATOR)) != string::npos)
        {
            token.push_back(recv_msg.substr(0, pos));
            recv_msg.erase(0, pos + strlen(_BUCKET_ETC_SEPARATOR));
        }
        token.push_back(recv_msg);

        if (!token.empty() && token.size() == 2)
        {
            if (_SHARE_CLASS::_WORK_CLASS && _extractor_class)
            {
                f_size = _extractor_class->work(token.at(0).c_str(), token.at(1).c_str());
                return_msg = to_string(f_size);
                if (f_size < 0)
                {
                    return_msg += _BUCKET_RETURN_SEPARATOR + to_string((int)_extractor_class->getLastError()) + _BUCKET_RETURN_SEPARATOR + _DEFAUKT_ERROR_MESSAGE;
                }
            }
            else if (_extractor)
            {
                f_size = _extractor->work(token.at(0).c_str(), token.at(1).c_str());
                return_msg = to_string(f_size);
                if (f_size < 0)
                {
                    return_msg += _BUCKET_RETURN_SEPARATOR + to_string((int)_extractor->getLastError()) + _BUCKET_RETURN_SEPARATOR + _DEFAUKT_ERROR_MESSAGE;
                }   
            }
        }

        return_msg += "\n";

        return return_msg;
    };

    void socketThread::run(void)
    {
        bool isalive = strcmp(_SHARE_CLASS::_SERVER_COMMAND, _SHUTDOWN_COMMAND) != 0 && (_sockfd > 0);

        if (strcmp(_SHARE_CLASS::_SERVER_COMMAND, _SHUTDOWN_COMMAND) == 0 && _sockfd > 0)
        {
            string send_msg = "shutdown server";
            send(_sockfd, send_msg.c_str(), strlen(send_msg.c_str()), 0);
        }

        while (isalive)
        {
            char buffer[_BUFFER_SIZE + 1];
            std::string recv_msg = "";
            int recv_len = -1;
            bool iscontinue = true;

            while (iscontinue)
            {
                memset(buffer, 0x00, sizeof(char) * (_BUFFER_SIZE + 1));
                recv_len = recv(_sockfd, buffer, _BUFFER_SIZE, 0);
                if (recv_len > 0)
                {
                    if (buffer[recv_len - 1] == '\n')
                    {
                        buffer[recv_len - 1] = 0x00;
                        iscontinue = false;
                    }
                    recv_msg += buffer;
                }
                else
                {
                    iscontinue = false;
                }
            }

            if (strcmp(recv_msg.c_str(), _CLOSE_MESSAGE) == 0)
            {
                isalive = false;
                std::cout << "recv close msg" << std::endl;
            }
            else if (strcmp(recv_msg.c_str(), _SHUTDOWN_COMMAND) == 0)
            {
                _SHARE_CLASS::_SERVER_COMMAND = _SHUTDOWN_COMMAND;
                isalive = false;
                std::cout << "shutdown msg" << std::endl;
            }
            else if (!recv_msg.empty())
            {
                string send_msg = textProcessing(recv_msg.c_str());
                send(_sockfd, send_msg.c_str(), strlen(send_msg.c_str()), 0);

                std::cout << "recv msg: " << recv_msg << std::endl;
                std::cout << "send msg: " << send_msg << std::endl;
            }            
        }
            
        emptyThreadObj::run();
    };
}