#include <sockethandler.h>
#include <socketThread.h>
#include <threadPool.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <iostream>

namespace hancyber::open::forensic
{
    socketHandler::socketHandler(int portno, int backlog, int threads): _portno(portno), _backlog(backlog), _threads(threads){};

    int socketHandler::start(void) const
    {
        int sockfd, acc_sockfd;
        socklen_t client;
        int sock_result;
        struct sockaddr_in server_addr, client_addr;
        sockfd = socket(AF_INET, SOCK_STREAM, 0);

        if (sockfd < 0)
        {
            std::cout << "socket file error" << std::endl;
            return static_cast<int>(ERROR_CODE::SOCKET_FD_ERROR);
        }

        memset(&server_addr, 0, sizeof(server_addr));
        server_addr.sin_family = AF_INET;
        server_addr.sin_addr.s_addr = INADDR_ANY;
        server_addr.sin_port = htons(_portno);

        sock_result = bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));

        if (sock_result < 0)
        {
            std::cout << "socket bind error" << std::endl;
            return static_cast<int>(ERROR_CODE::SOCKET_BIND_ERROR);
        }

        sock_result = listen(sockfd, _backlog);

        if (sock_result < 0)
        {
            std::cout << "socket server error" << std::endl;
            return static_cast<int>(ERROR_CODE::SOCKET_SVR_ERROR);
        }

        std::cout << "socket server listening" << std::endl;

        client = sizeof(client_addr);

        threadPool* pool = new threadPool(_threads);
        if (pool && pool->init())
        {
            while (iscontinue())
            {
                acc_sockfd = accept(sockfd, (struct sockaddr*)&client_addr, &client);
                
                if (acc_sockfd < 0)
                {
                    std::cout << "socket client error" << std::endl;
                }
                else
                {
                    std::cout << "socket accepted: " << inet_ntoa(client_addr.sin_addr) << std::endl;
                    
                    abThreadObj* thread = new socketThread(acc_sockfd);
                    pool->add(thread);
                }
            }
            //if (!pool->isFinish())
            //{
                //pool->setForceExit(true);
            //}
            while (!pool->isFinish()){}
            delete pool;
            pool = NULL;
        }

        close(sockfd);
        
        return static_cast<int>(ERROR_CODE::NONE);
    };

    bool socketHandler::iscontinue(void) const
    {
        return strcmp(_SHARE_CLASS::_SERVER_COMMAND, _SHUTDOWN_COMMAND) != 0;
    };
}