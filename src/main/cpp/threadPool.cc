/**************************************************************************************************
스레드풀 구현
create date : 2014.08.04 by hancyber
last modify date : 2017.01.20 by hancyber

--2017.01.20
threadPool::isFinish() mutex 구간변경
**************************************************************************************************/

#include "threadPool.h"

#include <sys/time.h>
#include <unistd.h>
//#include <algorithm>
#include <iostream>

using namespace std;

namespace hancyber
{
	namespace lib
	{
		namespace thread
		{
			bool isFinish(abThreadObj* obj)
			{
				return (obj->isFinish());
			};
			void* waitFunc(void* arg)
			{
				threadPool* th = (threadPool*)arg;
				while(!th->getForceExit() && th->isWait())
				{
					th->waitCheck();
					usleep(THREAD_LOOP_SLEEP);
				}
				//pthread_exit(0);
				return NULL;
			};
			void* threadFunc(void* arg)
			{
				abThreadObj* th = (abThreadObj*)arg;
				th->run();
				return NULL;
			};



			threadPool::threadPool(const int cnt)
			: _size(cnt)
			, _is_wait(true)
			, _is_block(false)
			, _is_force_exit(false)
			, _is_pause(false)
			, _pool(new vector<abThreadObj*>(0))
			, _wait(new vector<abThreadObj*>(0))
			, _wait_thread(NULL)
			,_pool_mutex(PTHREAD_MUTEX_INITIALIZER)
			{};
			threadPool::~threadPool()
			{
				dispose();
			};
			void threadPool::dispose()
			{
				disposeWait();
				while(!disposePool())
				{
					usleep(THREAD_LOOP_SLEEP);
				};
				if (_pool)
				{
					_pool->clear();
					delete _pool;
					_pool = NULL;
				}
				if (_wait)
				{
					for (vector<abThreadObj*>::iterator it = _wait->begin(); it != _wait->end(); ++it)
					{
						if ((*it))
						{
							delete (*it);
							(*it) = NULL;
						}
					}
					_wait->clear();
					delete _wait;
					_wait = NULL;
				}
				pthread_mutex_destroy(&_pool_mutex);
			};
			void threadPool::disposeWait()
			{
				_is_wait = false;

				int status;
				try
				{
					pthread_join(_wait_thread, (void**)&status);
				}
				catch(exception& ex)
				{
					#ifdef _DEBUG
					cerr << ex.what() << endl;
					#endif
				}
			};
			bool threadPool::disposePool()
			{
				bool ret = true;
				pthread_mutex_lock(&_pool_mutex);
				if (_pool && _pool->size() > 0)
				{
					for (vector<abThreadObj*>::iterator it = _pool->begin(); it != _pool->end(); ++it)
					{
						if ((*it))
						{
							//if (_is_force_exit || (*it)->isFinish())
							if ((*it)->isFinish())
							{
								delete (*it);
								(*it) = NULL;
							}
							else
							{
								ret = false;
							}
						}
					}
				}
				pthread_mutex_unlock(&_pool_mutex);
				return ret;
			};
			void threadPool::execute(abThreadObj* obj)
			{
				pthread_t thread_obj;
				try
				{
					pthread_create(&thread_obj, NULL, threadFunc, obj);
					pthread_detach(thread_obj);
				}
				catch(exception& ex)
				{
					#ifdef	_DEBUG
					cerr << ex.what() << endl;
					#endif
				}
			};
			bool threadPool::init()
			{
				pthread_mutex_lock(&_pool_mutex);
				for(int i = 0; (i < _size) && ((int)_pool->size() < _size); ++i)
				{
					_pool->push_back(new emptyThreadObj());
					_pool->at(i)->run();
				}
				pthread_mutex_unlock(&_pool_mutex);
				try
				{
					pthread_create(&_wait_thread, NULL, waitFunc, this);
					//pthread_detach(_wait_thread);
					return true;
				}
				catch(exception& ex)
				{
					#ifdef	_DEBUG
					cerr << ex.what() << endl;
					#endif
					return false;
				}
			};
			void threadPool::waitCheck()
			{
				if (!_is_pause)
				{
					pthread_mutex_lock(&_pool_mutex);
					for (vector<abThreadObj*>::iterator it = _pool->begin(); it != _pool->end(); ++it)
					{
						if ((*it) != NULL && (*it)->isFinish())
						{
							if ((int)_wait->size() > 0)
							{
								delete (*it);
								(*it) = NULL;
								(*it) = _wait->front();
								execute((*it));
								_wait->erase(_wait->begin());
								vector<abThreadObj*>* temp_vec = _wait;
								_wait->swap(*temp_vec);
							}
							else if ((*it)->threadID() == THREADID::DERIVED)
							{
								delete (*it);
								(*it) = NULL;
								_wait->push_back(new emptyThreadObj());
								(*it) = new emptyThreadObj();
								(*it)->run();
							}
						}
					}
					pthread_mutex_unlock(&_pool_mutex);
				}
			};
			bool threadPool::isWait() const
			{
				return _is_wait;
			};
			const abThreadObj* threadPool::add(abThreadObj* obj)
			{
				if (!_is_block && !_is_force_exit)
				{
					pthread_mutex_lock(&_pool_mutex);
					if ((int)_pool->size() < _size && !_is_pause)
					{
						_pool->push_back(obj);
						execute(obj);
					}
					else
					{
						_wait->push_back(obj);
					}
					pthread_mutex_unlock(&_pool_mutex);
				}
				else
				{
					#ifdef _DEBUG
					cerr << "blocked" << endl;
					#endif
					delete obj;
					obj = NULL;
				}
				return obj;
			};
			void threadPool::wait(double timeout)
			{
				_is_block = true;

				pthread_mutex_lock(&_pool_mutex);
				int wait_obj_cnt = (int)_wait->size();
				pthread_mutex_unlock(&_pool_mutex);

				struct timeval start, end;
				gettimeofday(&start, NULL);
				double elapsed = -1;

				while(!_is_force_exit && (elapsed < timeout) && (wait_obj_cnt > 0))
				{
					pthread_mutex_lock(&_pool_mutex);
					wait_obj_cnt = (int)_wait->size();
					pthread_mutex_unlock(&_pool_mutex);
					usleep(THREAD_LOOP_SLEEP);
					if (timeout > 0)
					{
						gettimeofday(&end, NULL);
						elapsed = (double)(end.tv_sec) * (double)(1000000.0) + (double)(end.tv_usec) - (double)(start.tv_sec) * (double)(1000000.0) - (double)(start.tv_usec);
					}
				}
			};
			void threadPool::setForceExit(bool is_force_exit)
			{
				_is_force_exit = is_force_exit;
			};
			bool threadPool::getForceExit() const
			{
				return _is_force_exit;
			};
			void threadPool::setPause(bool is_pause)
			{
				_is_pause = is_pause;
			};
			bool threadPool::getPause() const
			{
				return _is_pause;
			};
			bool threadPool::isFinish()
			{
				pthread_mutex_lock(&_pool_mutex);
				bool is_finish = false;
				if (_is_force_exit || (int)_wait->size() == 0)
				{
					is_finish = true;
					for (vector<abThreadObj*>::iterator it = _pool->begin(); it != _pool->end(); ++it)
					{
						if ((*it) != NULL && !(*it)->isFinish())
						{
							is_finish = false;
							break;
						}
					}
				}
				else
				{
					is_finish = false;
				}
				pthread_mutex_unlock(&_pool_mutex);

				return is_finish;
			};
		}
	}
}