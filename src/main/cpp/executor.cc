#include <sockethandler.h>
#include <hdfswrapper.h>
#include <snfwrapper.h>
#include <iostream>
#include <string.h>

using namespace hancyber::open::forensic;



int main(int argc, const char** argv)
{
    int ret_code = static_cast<int>(ERROR_CODE::NONE);
    
    if (argc > 1 && strcmp(argv[1], "wc") == 0)
    {
        _SHARE_CLASS::_WORK_CLASS = true;
        std::cout << "start with workclass" << std::endl;
        if (argc > 3)
        {
            _SHARE_CLASS::_HDFS_NAMENODE = argv[2];
            _SHARE_CLASS::_HDFS_USERNAME = argv[3];
            std::cout << "HDFS namenode: " << _SHARE_CLASS::_HDFS_NAMENODE << std::endl;
            std::cout << "HDFS username: " << _SHARE_CLASS::_HDFS_USERNAME << std::endl;
        }
    }
    else
    {
        _SHARE_CLASS::_WORK_CLASS = false;
    }
    
    snf::snfWrapper* snf = snf::snfWrapper::getInstance();
    if (snf)
    {
        socketHandler* sock = new socketHandler();
        if (sock)
        {
            ret_code = sock->start();
            delete sock;
            sock = NULL;
        }
        snf->release();
        snf = NULL;
    }

    std::cout << "exit code: " << ret_code << std::endl;

    return ret_code;
};