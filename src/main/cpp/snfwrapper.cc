#include <snfwrapper.h>

#include <string.h>
#include <iostream>

namespace hancyber::open::forensic::snf
{
    snfWrapper* snfWrapper::_instance = NULL;
    pthread_mutex_t snfWrapper::_singleton_mutex = PTHREAD_MUTEX_INITIALIZER;
    int snfWrapper::_ref = 0;

    snfWrapper* snfWrapper::getInstance(void)
    {
        pthread_mutex_lock(&_singleton_mutex);
        if (_ref++ == 0 && _instance == NULL)
        {
            _instance = new snfWrapper();
            _instance->init();
        }
        pthread_mutex_unlock(&_singleton_mutex);

        return _instance;
    };
    void snfWrapper::init(void)
    {
        sn3gbl_setcfg(_PKEY, SN3FILETYPE_ALL, SN3OPTION_EXTENSION_NO_CHECK, _bsize);
    };
    void snfWrapper::release(void)
    {
        pthread_mutex_lock(&_singleton_mutex);
        if (_instance && --_ref == 0)
        {
            delete _instance;
            _instance = NULL;
        }
        pthread_mutex_unlock(&_singleton_mutex);
    };

    __finfo* snfWrapper::filter(__uint8* buf, size_t buflen) const
    {
        pthread_mutex_lock(&_singleton_mutex);
        SN3MFI* pMFI;
        SN3BUF* pBuf;
        __finfo* finfo = NULL;

        int ret = sn3mfi_fopen_m(buf, (__int64)buflen, &pMFI);
        if (ret == SN3OK)
        {
            ret = sn3buf_init(&pBuf);
            if (ret == SN3OK) 
            {
                ret = sn3flt_filter_m(pMFI, pBuf, 1);
                if (ret == SN3OK)
                {
                    size_t datalen = snf_buf_get_utf8_len(pBuf);
                    __uint8* data = new __uint8[datalen + 1];
                    memset(data, 0, sizeof(unsigned char) * (datalen + 1));
                    int b_size = snf_buf_get_text(pBuf, data, static_cast<int>(datalen), SN3UCS_UTF8);
                    
                    finfo = new __finfo(data, b_size);
                }
                sn3buf_free(pBuf);
            }
            sn3mfi_fclose(pMFI);
        }
        pthread_mutex_unlock(&_singleton_mutex);

        return finfo;
    };
}