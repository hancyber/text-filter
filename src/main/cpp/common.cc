#include <common.h>

namespace hancyber::open::forensic
{
    bool _SHARE_CLASS::_WORK_CLASS = false;
    const char* _SHARE_CLASS::_HDFS_NAMENODE = _DEFAULT_NAMENODE;
    const char* _SHARE_CLASS::_HDFS_USERNAME = _DEFAULT_USERNAME;
    const char* _SHARE_CLASS::_SERVER_COMMAND = _DEFAULT_COMMAND;
};