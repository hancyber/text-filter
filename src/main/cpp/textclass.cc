#include <textclass.h>

#include <string.h>
#include <iostream>
#include <fstream>

namespace hancyber::open::forensic
{
    textClass::textClass(void):
    _hdfs_class(new hdfs::hdfsClass()),
    _snf_wrapper(snf::snfWrapper::getInstance()),
    _err_code(ERROR_CODE::NONE)
    {};

    textClass::~textClass(void)
    {
        if (_hdfs_class)
        {
            delete _hdfs_class;
            _hdfs_class = NULL;
        }
        if (_snf_wrapper)
        {
            _snf_wrapper->release();
        }
    };

    __finfo* textClass::read(const char* src) const
    {
        if (_hdfs_class)
        {
            hdfsFS fs = _hdfs_class->gethdfsFS();
            if (fs)
            {
                hdfsStreamBuilder* sb = hdfsStreamBuilderAlloc(fs, src, O_RDONLY);
                if (sb)
                {
                    if (hdfsExists(fs, src) == 0)
                    {
                        hdfsFile fh = hdfsStreamBuilderBuild(sb);
                        
                        int full_size = hdfsAvailable(fs, fh);
                        unsigned char* buffer = new unsigned char[full_size + 1];
                        memset(buffer, 0x00, sizeof(char) * (full_size + 1));
                        uint64_t total_size = 0;
                        tSize buffer_size = _READ_BUFFER_SIZE;
                        tSize read_bytes = buffer_size;
                        bool is_first = true;
                        while (read_bytes == buffer_size)
                        {
                            unsigned char* tmp_buffer = new unsigned char[buffer_size];
                            memset(tmp_buffer, 0x00, sizeof(unsigned char) * (buffer_size));
                            read_bytes = hdfsRead(fs, fh, tmp_buffer, buffer_size);
                            if (read_bytes > 0)
                            {
                                for (int i = 0;i < read_bytes; ++i)
                                {
                                    buffer[total_size + i] = tmp_buffer[i];
                                }
                                /*
                                원하는 동작이 되지 않음...
                                임시로 위의 코드로 대처함
                                if (is_first)
                                {
                                    strcpy((char*)buffer, (const char*)tmp_buffer);
                                    is_first = false;
                                }
                                else
                                {                                                  
                                    strcat((char*)buffer, (const char*)tmp_buffer);
                                }
                                */
                                total_size += read_bytes;
                            }
                            delete[] tmp_buffer;
                        }

                        hdfsCloseFile(fs, fh);

                        __finfo* finfo = new __finfo(buffer, total_size);

                        return finfo;
                    }
                    else
                    {
                        hdfsStreamBuilderFree(sb);
                    }                    
                }
            }
        }
        
        return NULL;        
    };

    int textClass::write(const char* dst, unsigned char* data, int f_size) const
    {
        int ret = 0;
        if (_hdfs_class)
        {
            hdfsFS fs = _hdfs_class->gethdfsFS();
            if (fs)
            {
                hdfsStreamBuilder* sb = hdfsStreamBuilderAlloc(fs, dst, O_WRONLY);
                if (sb)
                {
                    hdfsFile fh = hdfsStreamBuilderBuild(sb);
                    ret = hdfsWrite(fs, fh, data, f_size);
                    hdfsCloseFile(fs, fh);                    
                }
            }
        }

        return ret; 
    };

    int textClass::work(const char* src, const char* dst)
    {
        int ret = -1;
        _err_code = ERROR_CODE::NONE;

        __finfo* hdata = read(src);
        if (hdata)
        {
            if (hdata->size > 0)
            {
                __finfo* sdata = _snf_wrapper->filter(static_cast<__uint8*>(hdata->data), hdata->size);
                if (sdata && sdata->size > 0)
                {
                    ret = write(dst, sdata->data, sdata->size);
                    delete sdata;
                }
                else
                {
                    _err_code = ERROR_CODE::SYNAP_FILTER_FAIL;
                }                
            }
            else
            {
                _err_code = ERROR_CODE::HADOOP_SIZE_ZERO;
            }            

            delete hdata;
        }
        else
        {
            _err_code = ERROR_CODE::HADOOP_READ_FAIL;
        }

        return ret;
    };

    ERROR_CODE textClass::getLastError(void) const
    {
        return _err_code;
    };

    void* textClass::test(const char* src, const char* dst)
    {
        int w_size = -1;
        if ((w_size = work(src, dst)) > 0)
        {
            std::cout << w_size << std::endl;
        }
        else
        {
            std::cout << (unsigned long)getLastError() << std::endl;
        }        
    };
}