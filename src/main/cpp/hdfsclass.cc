#include <hdfsclass.h>

namespace hancyber::open::forensic::hdfs
{

    hdfsClass::hdfsClass(const char* namenode, const char* username):_fs(NULL)
    {
        init(namenode, username);
    };
    hdfsClass::~hdfsClass(void)
    {
        if (_fs)
        {
            hdfsDisconnect(_fs);
            _fs = NULL;
        }
    };
    
    void hdfsClass::init(const char* namenode, const char* username)
    {
        hdfsBuilder* builder = hdfsNewBuilder();
        if (builder)
        {
            hdfsBuilderSetNameNode(builder, namenode);
            hdfsBuilderSetUserName(builder, username);
            _fs = hdfsBuilderConnect(builder);
        }
    };

    const hdfsFS hdfsClass::gethdfsFS(void) const
    {
        return _fs;
    };
}