#ifndef SOCKETHANDLER_H
#define SOCKETHANDLER_H



#ifndef _DEFAULT_PORTNO
#define _DEFAULT_PORTNO 10070
#endif

#ifndef _DEFAULT_BACKLOG
#define _DEFAULT_BACKLOG 50
#endif

#ifndef _DEFAULT_THREAD
#define _DEFAULT_THREAD 50
#endif

#ifndef _MIN_THREAD
#define _MIN_THREAD 1
#endif

namespace hancyber::open::forensic
{
    class socketHandler
    {
        private:
            int _backlog = 0;
            int _portno = 0;
            int _threads = 0;
            bool iscontinue(void) const;
        public:
            socketHandler(int = _DEFAULT_PORTNO, int = _DEFAULT_BACKLOG, int = _MIN_THREAD);
            int start(void) const;
    };
}

#endif