#ifndef HDFSWRAPPER_H
#define HDFSWRAPPER_H

#include <pthread.h>
#include <common.h>

#include "/home/hduser/hadoop/include/hdfs.h"

#ifndef _READ_BUFFER_SIZE
#define _READ_BUFFER_SIZE 1024 * 1024
#endif

namespace hancyber::open::forensic::hdfs
{
    class hdfsWrapper
    {
        private:
            hdfsWrapper(void):_fs(NULL){};
            void init(const char*, const char*);
            static hdfsWrapper* _instance;
            static pthread_mutex_t _singleton_mutex;
            static int _ref;
            hdfsFS _fs;

        public:
            static hdfsWrapper* getInstance(const char* = _DEFAULT_NAMENODE, const char* = _DEFAULT_USERNAME);
            void release(void);
            const hdfsFS gethdfsFS(void) const;

            __finfo* read(const char* src) const;
            int write(const char* dst, unsigned char* data, int f_size) const;
    };
}

#endif