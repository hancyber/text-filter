#ifndef SNFWRAPPER_H
#define SNFWRAPPER_H

#include <pthread.h>

#include "snf.h"
#include "common.h"

#ifndef _BASE_BUFFER_SIZE
#define _BASE_BUFFER_SIZE 1024 * 1024
#endif

namespace hancyber::open::forensic::snf
{
    class snfWrapper
    {
        private:
            snfWrapper(void){};
            void init(void);
            static snfWrapper* _instance;
            static pthread_mutex_t _singleton_mutex;
            static int _ref;
            const char* _PKEY = "";
            const size_t _bsize = _BASE_BUFFER_SIZE;

        public:
            static snfWrapper* getInstance(void);
            void release(void);
            __finfo* filter(__uint8*, size_t) const;
    };
}

#endif