/**************************************************************************************************
스레드 선언
create date : 2014.08.04 by hancyber
last modify date : 2014.09.26 by hancyber
**************************************************************************************************/
#ifndef	AB_THREADOBJ_H_
#define	AB_THREADOBJ_H_

#include <vector>

using namespace std;

namespace hancyber
{
	namespace lib
	{
		namespace thread
		{
			typedef enum THID
			{
				BASE,
				DERIVED
			}THREADID;

			class abThreadObj
			{
				protected:
					bool _isfinish;
				public:
					abThreadObj(void);
					virtual ~abThreadObj(void){};
					bool isFinish(void) const;
					virtual void setArg(void*, void* = NULL) = 0;
					virtual void run(void) = 0;
					virtual THREADID threadID(void) const;
			};
			class emptyThreadObj : public abThreadObj
			{
				public:
					virtual ~emptyThreadObj(void){};
					virtual void setArg(void*, void* = NULL){};
					virtual void run(void);
					virtual THREADID threadID(void) const;
			};
		}
	}
}



#endif /* AB_THREADOBJ_H_ */
