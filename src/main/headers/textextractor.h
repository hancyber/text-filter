#ifndef TEXTEXTRACTOR_H
#define TEXTEXTRACTOR_H

#include <common.h>
#include <hdfswrapper.h>
#include <snfwrapper.h>

#ifndef _DEFAUKT_ERROR_MESSAGE
#define _DEFAUKT_ERROR_MESSAGE "error"
#endif

#ifndef _READ_BUFFER_SIZE
#define _READ_BUFFER_SIZE 1024 * 1024
#endif

namespace hancyber::open::forensic
{
    class textExtractor
    {
        private:
            hdfs::hdfsWrapper* _hdfs_wrapper;
            snf::snfWrapper* _snf_wrapper;
            ERROR_CODE _err_code;
            __finfo* read(const char*) const;
            int write(const char*, unsigned char*, int) const;
        public:
            textExtractor(void);
            ~textExtractor(void);
            int work(const char*, const char*);
            ERROR_CODE getLastError(void) const;
            void* test(const char*, const char*);
    };
}

#endif