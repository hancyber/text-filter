#ifndef SOCKETTHREAD_H
#define SOCKETTHREAD_H

#include <threadPool.h>
#include <textextractor.h>
#include <textclass.h>

#include <string>



#ifndef _BUFFER_SIZE
#define _BUFFER_SIZE 512
#endif

#ifndef _CLOSE_MESSAGE
#define _CLOSE_MESSAGE "close"
#endif

#ifndef _BUCKET_ETC_SEPARATOR
#define _BUCKET_ETC_SEPARATOR "___#___"
#endif

#ifndef _BUCKET_RETURN_SEPARATOR
#define _BUCKET_RETURN_SEPARATOR ":::"
#endif

using namespace hancyber::lib::thread;
using namespace std;

namespace hancyber::open::forensic
{
    class socketThread: public emptyThreadObj
    {
        private:
            int _sockfd = -1;
            textExtractor* _extractor = NULL;
            textClass* _extractor_class = NULL;
			virtual THREADID threadID(void) const;
            const string textProcessing(string) const;
        public:
            socketThread(int);
            virtual ~socketThread(void);
            virtual void setArg(void* = NULL, void* = NULL){};
            virtual void run(void);
    };
}

#endif