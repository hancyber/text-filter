/**************************************************************************************************
스레드풀 선언
create date : 2014.08.04 by hancyber
last modify date : 2014.10.13 by hancyber
**************************************************************************************************/
#ifndef	THREAD_POOL_H_
#define	THREAD_POOL_H_

#include <pthread.h>
#include <vector>

#include <abThreadObj.h>



#ifndef	THREAD_LOOP_SLEEP
#define	THREAD_LOOP_SLEEP 1000*50
#endif
#ifndef	DEFAULT_THREAD_SIZE
#define	DEFAULT_THREAD_SIZE 10
#endif



using namespace std;

namespace hancyber
{
	namespace lib
	{
		namespace thread
		{
			bool isFinish(abThreadObj*);
			void* waitFunc(void*);
			void* threadFunc(void*);

			class threadPool
			{
				private:
					const int _size;
					bool _is_wait;
					bool _is_block;
					bool _is_force_exit;
					bool _is_pause;
					vector<abThreadObj*>* _pool;
					vector<abThreadObj*>* _wait;
					pthread_t _wait_thread;
					pthread_mutex_t _pool_mutex;
					void dispose(void);
					void disposeWait(void);
					bool disposePool(void);
					void execute(abThreadObj*);
				public:
					threadPool(const int = DEFAULT_THREAD_SIZE);
					~threadPool(void);
					bool init(void);
					void waitCheck(void);
					bool isWait(void) const;
					const abThreadObj* add(abThreadObj*);
					void wait(double = 0);
					void setForceExit(bool);
					bool getForceExit(void) const;
					void setPause(bool);
					bool getPause(void) const;
					bool isFinish(void);
			};
		}
	}
}



#endif
