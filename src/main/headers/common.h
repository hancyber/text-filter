#ifndef COMMON_H
#define COMMON_H



#ifndef _DEFAULT_NAMENODE
#define _DEFAULT_NAMENODE "hdfs://open"
#endif

#ifndef _DEFAULT_USERNAME
#define _DEFAULT_USERNAME "open"
#endif

#ifndef _DEFAULT_COMMAND
#define _DEFAULT_COMMAND "none"
#endif

#ifndef _SHUTDOWN_COMMAND
#define _SHUTDOWN_COMMAND "server.shutdown"
#endif

namespace hancyber::open::forensic
{
    typedef struct __fileinfo
    {
        __fileinfo(unsigned char* content, int len): data(content), size(len){}
        ~__fileinfo(void){if(data)delete[] data;};
        unsigned char* data;
        int size;
    }__finfo;

    typedef enum class __error: unsigned long
    {
        NONE = 0x00ul,
        ERROR = 0x01ul,
        
        DEFAULT_ERROR = ERROR | ERROR << 1,

        SOCKET_ERROR = ERROR | ERROR << 2,
        SOCKET_FD_ERROR = SOCKET_ERROR | ERROR << 3,
        SOCKET_BIND_ERROR = SOCKET_ERROR| ERROR << 4,
        SOCKET_SVR_ERROR = SOCKET_ERROR| ERROR << 5,
        SOCKET_CLI_ERROR = SOCKET_ERROR| ERROR << 6,

        HADOOP_ERROR = ERROR | ERROR << 10,
        HADOOP_READ_FAIL = HADOOP_ERROR | ERROR << 11,
        HADOOP_SIZE_ZERO = HADOOP_ERROR | ERROR << 12,
        HADOOP_WRITE_FAIL = HADOOP_ERROR | ERROR << 13,

        SYNAP_ERROR = ERROR | ERROR << 20,                
        SYNAP_FILTER_FAIL = SYNAP_ERROR | ERROR << 21,
    }ERROR_CODE;

    class _SHARE_CLASS
    {
        public:
            static bool _WORK_CLASS;
            static const char* _HDFS_NAMENODE;
            static const char* _HDFS_USERNAME;
            static const char* _SERVER_COMMAND;
    };
};

#endif