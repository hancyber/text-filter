#ifndef HDFSCLASS_H
#define HDFSCLASS_H

#include "/home/hduser/hadoop/include/hdfs.h"
#include <common.h>

namespace hancyber::open::forensic::hdfs
{
    class hdfsClass
    {
        private:
            void init(const char*, const char*);
            hdfsFS _fs;
        public:
            hdfsClass(const char* = _DEFAULT_NAMENODE, const char* = _DEFAULT_USERNAME);
            ~hdfsClass(void);
            const hdfsFS gethdfsFS(void) const;
    };
}

#endif